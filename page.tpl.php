<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <!--[if IE 6]>
  <style type="text/css" media="all">@import "<?php print base_path().$directory ?>/ie6.css";</style>  
  <![endif]-->
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

	<div id="wrapper">

		<div id="header">
		<a href="<?php print check_url($base_path); ?>"><?php if ($logo) { print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />'; } ?></a>
			<?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
			<?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
			<!--?php print $search_box ?-->
		</div>

		<div id="menu">
			<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
		</div>
		
		<div id="content">
			<?php if ($header) { ?><div id="top-header"><?php print $header ?></div><?php } ?>

			<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
			<div id="main">
				<?php print $breadcrumb ?>
				<h1 class="title"><?php print $title ?></h1>
				<div class="tabs"><?php print $tabs ?></div>
				<?php print $help ?>
				<?php print $messages ?>
				<?php print $content; ?>
				<?php print $feed_icons; ?>
			</div>
		</div>

		<?php if ($sidebar_right || $sidebar_left) { ?>
		<div id="sidebar">
			<div id="sidebarsearch">
				<?php print $search_box ?>
			</div>
						
				
			<?php if ($sidebar_right) { ?>
			<div id="sidebar-right">
				<?php print $sidebar_right ?>
				<div id="sidebar-bottom">
					&nbsp;
				</div>
			</div>
			<?php } ?>
			<?php if ($sidebar_left) { ?>
			<div id="sidebar-left">
				<div id="sidebar-top">
					&nbsp;
				</div>
				<?php print $sidebar_left ?>
				<div id="sidebar-bottom">
					&nbsp;
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
		<div id="footer">

			<div id="footer-valid">
			<?php print $footer_message ?>
			</div>
			<div id="credit">
		<a href="http://drupal.org/project/ufutbol">uFutbol theme</a> designed and developed by <a href="http://www.softwarelibre.net">Bustillo / Software Libre</a> for <a href="http://www.ufutbol.com">uFútbol</a>
			</div>

		</div>
		<?php print $closure ?>
	</div>
</body>
</html>
